(ns all-challenges.maximum-length-difference-test
  (:require [clojure.test :refer :all]
            [all-challenges.maximum-length-difference :refer :all]))

(deftest max-len-diff-test
  (testing "function returns -1 when given empty lists"
    (def a1 '())
    (def a2 '("i didnt come empty handed"))
    (is (= (max-len-diff a1 a2) -1)))

  (testing "function returns length difference"
    (def a1 '("hello" "pugs" "nacho" "thebatmanpug"))
    (def a2 '("jon" "jason" "joe" "sean" "raymond"))
    (is (= (max-len-diff a1 a2) 5))))
