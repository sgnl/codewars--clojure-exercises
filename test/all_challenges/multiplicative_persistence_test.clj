(ns all-challenges.multiplicative-persistence-test
  (:require [clojure.test :refer :all]
            [all-challenges.multiplicative-persistence :refer :all]))

(deftest multiplicative-persistence-test
  (testing "return multiplicative persistence of a given value"
    (is (= (multi-persist 1) 0))
    (is (= (multi-persist 9) 0))
    (is (= (multi-persist 39) 3))
    (is (= (multi-persist 4) 0))
    (is (= (multi-persist 25) 2))
    (is (= (multi-persist 999) 4))))

