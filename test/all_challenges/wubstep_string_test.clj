(ns all-challenges.wubstep-string-test
  (:require [clojure.test :refer :all]
            [all-challenges.wubstep-string :refer :all]))

(deftest wubstep-string-suite
  (testing "function returns -1 when given empty lists"
    (is (= (song-decoder "AWUBBWUBC") "A B C"))
    (is (= (song-decoder "AWUBWUBWUBBWUBWUBWUBC") "A B C"))
    (is (= (song-decoder "WUBAWUBBWUBCWUB") "A B C"))

    (is (= (song-decoder "RWUBWUBWUBLWUB") "R L"))
    (is (= (song-decoder "WUBJKDWUBWUBWBIRAQKFWUBWUBYEWUBWUBWUBWVWUBWUB") "JKD WBIRAQKF YE WV"))
    (is (= (song-decoder "WUBKSDHEMIXUJWUBWUBRWUBWUBWUBSWUBWUBWUBHWUBWUBWUB") "KSDHEMIXUJ R S H"))
    (is (= (song-decoder "QWUBQQWUBWUBWUBIWUBWUBWWWUBWUBWUBJOPJPBRH") "Q QQ I WW JOPJPBRH"))
    (is (= (song-decoder "WUBWUBOWUBWUBWUBIPVCQAFWYWUBWUBWUBQWUBWUBWUBXHDKCPYKCTWWYWUBWUBWUBVWUBWUBWUBFZWUBWUB") "O IPVCQAFWY Q XHDKCPYKCTWWY V FZ"))
    (is (= (song-decoder "WUBYYRTSMNWUWUBWUBWUBCWUBWUBWUBCWUBWUBWUBFSYUINDWOBVWUBWUBWUBFWUBWUBWUBAUWUBWUBWUBVWUBWUBWUBJB") "YYRTSMNWU C C FSYUINDWOBV F AU V JB"))
    (is (= (song-decoder "WUBKSDHEMIXUJWUBWUBRWUBWUBWUBSWUBWUBWUBHWUBWUBWUB") "KSDHEMIXUJ R S H"))
    (is (= (song-decoder "AWUBWUBWUB") "A"))
    (is (= (song-decoder "AWUBBWUBCWUBD") "A B C D"))
    (is (= (song-decoder "WUBWWUBWUBWUBUWUBWUBBWUB") "W U B"))
    (is (= (song-decoder "WUWUBBWWUBUB") "WU BW UB"))
    (is (= (song-decoder "WUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUABWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUBWUB") "WUAB"))
    (is (= (song-decoder "U") "U"))
    (is (= (song-decoder "WUWUB") "WU"))
    (is (= (song-decoder "UBWUB") "UB"))
    (is (= (song-decoder "WUWUBUBWUBUWUB") "WU UB U"))
    (is (= (song-decoder "WUBWWUBAWUB") "W A"))
    (is (= (song-decoder "WUUUUU") "WUUUUU"))
    (is (= (song-decoder "WUBWUBA") "A"))))
