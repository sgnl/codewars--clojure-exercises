(ns all-challenges.maximum-length-difference)

(defn max-len-diff
  "test"
  [x y]

  (defn maxStrLength
    "reduce to count then find max number"
    [arr]
    (reduce max (map count arr)))

  (if (or (empty? x) (empty? y))
    -1
    (Math/abs (- (maxStrLength x) (maxStrLength y)))))
