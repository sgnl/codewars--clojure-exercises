(ns all-challenges.wubstep-string
  (:require [clojure.string :as str]))

(defn song-decoder
  "remove the wubs"
  [song]
  (str/join " " (remove empty? (str/split song #"(WUB)+"))))
