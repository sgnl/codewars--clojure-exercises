(ns all-challenges.multiplicative-persistence)
(require '[clojure.string :as str])

(defn multi-persist
  "compute multiplicative persistence of a value"
  ([num] (multi-persist num 0))
  ([num steps]
   (if (< num 10)
     steps
     (recur (reduce * (map #(Integer/parseInt %1) (str/split (str num) #""))) (+ steps 1)))))

